/**
 * A ZeroMQ based client for the ``hps.servers.ZmqServer`` class
 */

import org.zeromq.*;

class ZmqClient {
    /**
     * Client class for the ZmqServer in the hps package
     */
    ZMQ.Context context;
    ZMQ.Socket request_socket;
    ZMQ.Socket pull_socket;
    String token;

    public ZmqClient(String server_host, int server_send_port, int server_recv_port, String client_id) {
        /**
         * @param host: The host name of the server
         * @param server_send_port: The port from which the server sends data. This will
         *          be the port on which the client receives data.
         * @param server_recv_port: The port on which the server receives data. This will
         *          be the port to which the client will send data.
         * @param client_id: An identifier for the client, which has to be
         *         unique for all clients connected to a single server.
         */

        context = ZMQ.context(1);

        pull_socket = context.socket(ZMQ.PULL);
        pull_socket.connect("tcp://"+server_host+":"+Integer.toString(server_send_port));

        request_socket = context.socket(ZMQ.REQ);
        request_socket.connect("tcp://"+server_host+":"+Integer.toString(server_recv_port));
        request_socket.send(client_id, 0);
        token = request_socket.recvStr(0);
    }

    String receive_data() {
        /**
         * Receive a message from the server
         *
         * @return The message received from the server as string
         */
        return pull_socket.recvStr(0);
    }

    String send_data(String data) {
        /**
         * Send data to the server
         *
         * The client will send the data to server until it gets a 200
         * response from the server. If the server does not respond
         * with a 202 or 200 status, an error is raised.
         *
         * @param data: The data to send to the server
         *
         * @return The reply send from the server as string
         */
        String reply = "202";
        while (!reply.startsWith("200")) {
            String to_send = token + " " + data;
            request_socket.send(to_send, 0);
            reply = request_socket.recvStr(0);
            if (!reply.startsWith("200") && !reply.startsWith("202")) {
                String err = "Expected 200 or 202 response. Server responded with " + reply + ".";
                throw new java.lang.RuntimeException(err);
            }
        }
        return reply.replaceFirst("200 ", "");
    }

    void close() {
        /**
         * Close the connections
         */
        request_socket.close();
        pull_socket.close();
        context.term();
    }
}
