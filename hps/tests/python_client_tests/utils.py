import multiprocessing
import traceback
from time import sleep


class Process(multiprocessing.Process):
    """A Process object that throws exception to the parent

    This object inherits from the ``multiprocessing.Process`` with the
    an additional check for exceptions. When exceptions are encountered
    they are send back to the parent process over a pipe and raised in
    the parent process
    """
    def __init__(self, *args, **kwargs):
        multiprocessing.Process.__init__(self, *args, **kwargs)
        self._pconn, self._cconn = multiprocessing.Pipe()
        self._exception = None

    def run(self):
        try:
            multiprocessing.Process.run(self)
            self._cconn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._cconn.send((e, tb))
            raise

    @property
    def exception(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception


def start_server(target_func):
    """Runs the server as a subprocess

    Args:
        :target_func: A callable that runs the server with the
            expected sends, receives and tests

    Return:
        A ``utils.Process`` object for the process running the
        server
    """
    server = Process(target=target_func)
    server.start()

    # sleep makes sure the server starts up before
    # the clients start making connections
    sleep(0.5)
    return server
