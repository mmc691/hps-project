import os
import subprocess
import sys
from time import sleep
import unittest

from utils import start_server


__this_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(__this_dir + '/../../')

from servers import ZmqServer
from clients.python.zmq_client import ZmqClient

class test_socket_comm(unittest.TestCase):

    HOST = '127.0.0.1'
    SEND_PORT = 9000
    RECV_PORT = 9001

    def test_vanilla_client(self):

        def vanilla_server():
            server = ZmqServer(self.HOST, self.SEND_PORT, self.RECV_PORT, 1)
            server.establish_client_connections()
            server.send_to_all('Hello')
            resp = server.receive_from_all()
            self.assertIsInstance(resp, list)
            self.assertEqual(resp[0]['data'], 'World')
            server.close()

        server = start_server(vanilla_server)
        client = ZmqClient(self.HOST, self.SEND_PORT, self.RECV_PORT, 'Client')
        data = client.receive_data()
        self.assertEqual(data, 'Hello')
        client.send_data('World')
        client.close()
        server.terminate()

    def test_many_clients(self):

        num_clients = 5

        def many_client_server():
            server = ZmqServer(self.HOST, self.SEND_PORT, self.RECV_PORT, num_clients)
            server.establish_client_connections()
            server.send_to_all('Hello')
            resp = server.receive_from_all()
            self.assertEqual(len(resp), num_clients)
            for r in resp:
                self.assertEqual(r['data'], 'World')
            server.close()

        server = start_server(many_client_server)
        clients = []
        for i in range(0, num_clients):
            clients.append(ZmqClient(self.HOST, self.SEND_PORT, self.RECV_PORT, 'Client %d' % i))
        data = [c.receive_data() for c in clients]
        self.assertEqual(data, ['Hello'] * num_clients)
        for c in clients:
            c.send_data('World')
        for c in clients:
            c.close()
        server.terminate()

    def test_client_request(self):

        num_clients = 3

        def client_request_server():
            server = ZmqServer(self.HOST, self.SEND_PORT, self.RECV_PORT, num_clients)
            server.establish_client_connections()
            server.send_to('Bar', 'Client 1')
            server.close()

        server = start_server(client_request_server)
        clients = []
        for i in range(0, num_clients):
            clients.append(ZmqClient(self.HOST, self.SEND_PORT, self.RECV_PORT, 'Client %d' % i))
        resp = clients[1].send_data('Foo')
        self.assertEqual(resp, 'Bar')
        for c in clients:
            c.close()
        server.terminate()

    def test_doc_samples(self):

        num_clients = 3

        def doc_samples_server():
            server = ZmqServer(self.HOST, self.SEND_PORT, self.RECV_PORT, num_clients)
            server.establish_client_connections()
            reqs = server.receive_from_all()
            for req in reqs:
                if req['client_id'][0] == 'A':
                    _ = server.send_to('foo', req['client_id'])
                else:
                    server.receive_from(req['client_id'])
            server.close()

        server = start_server(doc_samples_server)
        clients = []
        for i in range(0, num_clients):
            clients.append(ZmqClient(self.HOST, self.SEND_PORT, self.RECV_PORT, 'A %d' % i if i == 1 else 'B %d' % i))
        for c in clients:
            c.send_data('')
        for i in range(0, len(clients)):
            resp = clients[i].send_data('')
            if i == 1:
                self.assertEqual(resp, 'foo')
        for c in clients:
            c.close()
        server.terminate()


if __name__ == '__main__':
    unittest.main(exit=False)
