/** needs socket_client.class to be in same directory to run **/

public class socket_client_test {
    public static void main(String[] args) throws Exception {
        SocketClient test_client = new SocketClient("127.0.0.1", 9000);
        System.out.println("Client has connected to server");
        String sent_from_server = test_client.receive_data();
        System.out.println("Client has received " + sent_from_server);
        test_client.send_data("World");
        System.out.println("Client has sent World");
        test_client.close_socket();
        System.out.println("Client has closed socket");
    }
}
