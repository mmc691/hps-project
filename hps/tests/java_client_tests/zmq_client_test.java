/**  will need to change class path depending on where zmq lib is. requires
 zmq_client.class file to be in same directory **/

public class zmq_client_test {
    public static void main(String[] args) {
        ZmqClient test_client = new ZmqClient("127.0.0.1", 9000, 9001, "test_client");
        System.out.println("Client has connected to server");
        String sent_from_server = test_client.receive_data();
        System.out.println("Client has received " + sent_from_server);
        test_client.send_data("World");
        System.out.println("Client has sent World");
        test_client.close();
        System.out.println("Client has closed socket");
    }
}
