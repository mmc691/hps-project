"""
A sample game where the server picks the max bid from the clients.
The first client to win 3 bids is the winner of the auction.
"""

import json
from multiprocessing import Process
from random import randint
from time import sleep

from hps.servers import ZmqServer
from hps.clients import ZmqClient


HOST = '127.0.0.1'
SEND_PORT = 9000
RECV_PORT = 9001


class Player:

    def __init__(self, name):
        """Connect to server and send the name"""
        self.name = name
        self.client = ZmqClient(HOST, SEND_PORT, RECV_PORT, name)

    def play_game(self):
        """Send bids until game ends"""
        score = 0
        while True:
            num = randint(1, 100)
            print('%s: Bidding %d' % (self.name, num))

            # Data is always sent or received as strings
            # Numbers must be explicitly converted to strings
            # before sending and JSON must be deserialized
            self.client.send_data(str(num))
            resp = json.loads(self.client.receive_data())

            if resp['winner'] == self.name:
                print('%s: Yay! I won the round!' % self.name)
                score += 1
            if resp['game_over']:
                if score == 3:
                    print('%s: Yippee! I won the game' % self.name)
                exit(0)


class GameController:

    def __init__(self, num_players):
        """Receive player names and initialize game state"""
        self.num_players = num_players
        self.server = ZmqServer(HOST, SEND_PORT, RECV_PORT, num_players)
        self.server.establish_client_connections()
        self.player_scores = {}
        for player in self.server.client_ids:
            self.player_scores[player] = 0

    def play_game(self):
        """Accept bids until game ends and a player wins"""
        game_over = False
        while not game_over:
            moves = self.server.receive_from_all()

            max_bid = 0
            round_winner = None
            for move in moves:
                # Cast to int since all data is received as string
                if int(move['data']) > max_bid:
                    round_winner = move['client_id']
                    max_bid = int(move['data'])
            self.player_scores[round_winner] += 1
            if self.player_scores[round_winner] == 3:
                game_over = True

            # Objects must be converted to JSON or some other
            # string format before sending
            self.server.send_to_all(json.dumps({'winner': round_winner, 'game_over': game_over}))


def player_process(name):
    """Target for processes to run the players"""
    sleep(1) # This is required to make sure the server is ready when the client connects
    player = Player(name)
    player.play_game()


def main():
    num_players = 4

    player_pool = []
    for i in range(0, num_players):
        # Run each player as a separate process to simulate independent clients
        player_pool.append(Process(target=player_process, args=('Player %d' % i,)))
        player_pool[i].start()

    controller = GameController(num_players)
    controller.play_game()


if __name__ == '__main__':
    main()
