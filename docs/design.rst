====================
Communication Design
====================

This document describes the details of the communication design between the
client and server for each class of client-server pair. Game architects will
find this helpful for the usage of the server and providing the sample client
to the players. It also goes into the internals of the implementations and
is generally helpful for using the classes provided by the package.

See the `sample-games directory <../sample-games>`_ for a sample usage of the
client and server for a simple auction game.

SocketServer and SocketClient
-----------------------------

The ``SocketServer`` and ``SocketClient`` use simple TCP sockets for network
communication. The sockets are usually exposed as ``socket`` or ``sck``
attribute of the particular class and can be used directly for setting various
options on it. The communication pattern supported by these classes is a simple,
blocking, 1-to-1 communication. The server can only send/receive data to/from
one client at a time. This makes it really easy to reason about the communication
and prevent bugs due to concurrency.

.. image :: sockets.png

Sample Usage
++++++++++++

When the server is constructed, a socket is created and bound to the hostname/IP
and port provided in the constructor. A list called ``client_sockets`` is created
as a placeholder for the client sockets. This list is populated with actual
client sockets when ``establish_client_connections`` is called. In most cases,
the usage of the server will pretty much look like ::

    from hps.servers import SocketServer

    def is_first_player(init_data):
        return init_data[0] == '0'

    server = SocketServer('0.0.0.0', 9000, 2)
    server.establish_client_connections()
    init_client_data = server.receive_from_all()
    if is_first_player(init_client_data[1]):
        server.client_sockets.reverse()
    server.send_to('foo', 0)
    server.send_to('bar', 1)

The above code snippet creates a server that listens for client on all IP addresses
and accepts two client connections. It then connects with the clients and receives
some initial data from them. The ``receive_from_all`` method for the server returns
the data in the order in which they are saved in ``client_sockets`` list. The
``is_first_player`` function player checks whether the socket order needs to be reversed
and does so accordingly. Ideally, the order of sockets in ``client_sockets`` should
correspond to the order of the player moves for ease of use of the server.

The client code corresponding to the above snippet will be something like ::

    from hps.clients import SocketClient

    client = SocketClient('server.i.p.address', 9000)
    client.send_data('0')        # if first player else client.send_data('1')
    resp = client.receive()

The client constructor is similar to the server constructor, but connects to the
server's socket instead of binding to an address. The client socket is available
as ``client.sck`` for manipualtion and setting options. Note that both clients
need to send a message since the ``receive_from_all`` method blocks until it has
received message from both clients.

After this, the game will proceed by the server sending a reply to the client and
the client's sending a new message. The ``send_to`` method can be replaced by
``send_to_all`` if the same response has to be sent to both clients, or by ``send_file``
if the contents of a file have to be sent. Similarly, the client can replace
``receive`` with one of the other receive methods as described in `Client docs <./client.rst>`_
depending on the response being sent by the server.

Not be used if
++++++++++++++

* This client-server pair does not support broadcast or multi-cast communications.
  Do not use this if the server needs to send or receive messages concurrently to
  multiple clients.

* The communincation should always be blocking. While it is okay to modify the socket
  timeouts on either the client or the server, setting the sockets to non-blocking
  will lead to race conditions. The internals of the server and the client assume that
  the sockets are always blocking.


ZmqServer and ZmqClient
-----------------------

The ``ZmqServer`` and ``ZmqClient`` are meant for scenarios where the communication
is concurrent and there is not guarantee in which order the clients will send requests.

The communications combine two separate design patterns of ZeroMQ to provide both
1-to-1 and 1-to-many patterns. For the 1-to-1 pattern, the REQ-REP socket pairs are
used and for the 1-to-many pattern, the PUSH-PULL socket pairs are used. Both these sockets
use a different port on the same host. See the `ZeroMQ docs <https://api.zeromq.org/>`_,
the `pyzmq docs <https://pyzmq.readthedocs.io/en/latest/>`_ and the
`message patterns <http://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/basics.html>`_
for further details about ZeroMQ.

.. image :: zmq.png

Sample Usage
++++++++++++

When the server is constructed, a ZeroMQ context is created a PUSH socket and REP socket
are bound to the send port and the receive port respectively. An empty dictionary called
``client_ids`` is created as a placeholder for clients' ids and tokens. This dictionary
is populated when the ``establish_client_connections`` method is called. The usage for the
server will be something like ::

    from hps.servers import ZmqServer

    game_params = '1 2 3'
    server = ZmqServer('12.34.56.78', 9000, 9001, 5)
    server.establish_client_connections()
    server.send_to_all(game_params)
    client_moves = server.receive_from_all()

The above code snippert starts the server ready to accpet 5 clients, establishes connection
with them and sends them all the ``game_params`` string. After that it receives the moves
made by all players. The ``receive_from_all`` method returns the moves in order they were
received along with the time they were received.

The client code for above snippet will be ::

    from hps.clients import ZmqClient

    client = ZmqClient('12.34.56.78', 9000, 9001, 'Client 0')
    game_params = client.receive()
    client.send_data(move)

Converse to the server, when the client is created a PULL socket and a REQ socket
are connected to the server's PUSH socket and REP socket. Note that the order of ports in
the constructor for the client and the server is the same. After connecting with
the servers sockets, the ``client_id`` which is ``'Client 0'`` here, is sent to the server
and a token is received from the server. This token is automatically included in any request
made by the client.

In case there is a need to do 1-to-1 communication with the client without processing the
message sent by the client, the server side code will be ::

    reqs = server.receive_from_all()
    for req in reqs:
        if req['client_id'][0] == 'A':
            req = server.send_to('foo', req['client_id'])
        else:
            server.receive_from(req['client_id'])

The client side code for this will be ::

    resp = client.send_data('')
    resp = client.send_data('')

In the above example, the server send a message ``'foo'`` to any client whose id's first letter
is 'A' and a blank response to any other client. The client has to make two calls to send since
the first called to send is replied to when by the ``receive_from_all`` method and the second
call tells the server to reply with the response once ready. The ``receive`` call does not work
here since the ``receive`` call only listens on the PULL socket, which is meant for receiving
broadcasts from the server, not 1-to-1 responses.

It is necessay for 1-to-1 communication that the client send a request to the server to respond.
This is a constraint on the ZeroMQ REQ-REP communication pattern. The server's REP socket can
only send messages to a client REQ socket which has sent it a request. So, a client has to make
a request to initiate any 1-to-1 communication.

If the request needs to be processed in the above example, the code on the server side will be ::

    reqs = server.receive_from_all()
    for req in reqs
        if req['client_id'][0] == 'A':
            resp = 1 if req == 'bar' else 0
            server.send_to(str(resp), req['client_id'])
        else:
            resp = 1 if req == 'baz' else 0
            server.send_to(str(resp), req['client_id'])

The client side code for this will be ::

    from random import choice
    l = ['bar', 'baz']
    resp = client.send_data(choice(l))
    assert resp == '200'
    resp = client.send_data('')
    assert resp in ('bar', 'baz')

Not to be used if
+++++++++++++++++

* Do not use this server and client if only serial communication is required and there are no
  concurrent messages.
